<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/logo.png" />
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <title>Jajan Properti | About us</title>
  </head>
  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1c145f">
      <div class="container">
        <a class="navbar-brand" href="#"> <img src="img/logo.png" width="90" height="50" class="d-inline-block align-top" alt="" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="beranda.php">Buy<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="rent.php">Rent</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="addproperti.php">Input Properties</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Others </a>
              <div class="dropdown-menu text-light" style="background-color: #1c145f" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item text-light" href="aboutus.html">About Us</a>
                <a class="dropdown-item text-light" href="panduan_pengguna.php">User Manual</a>
                <a class="dropdown-item text-light" href="#">Report and Feedback</a>
              </div>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-3" type="search" placeholder="Search" aria-label="Search" />
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->
    <!-- Jumbotorn -->
    <div class="jumbotron jumbotron-fluid">
      <div class="container text-center border">
        <img src="img/kantor.jpg" height="500rem" class="img-fluid" />
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h3>Jajan Properti</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-12 py-4 text-end text-center">
          <h3>Visi</h3>
          <p>
            Jajan Properti menyediakan banyak pilihan properti yang terpercaya, aman dan terjangkau. bersamaan dengan moto kami yaitu membangun dan menjembatani bisnis properti agar lebih mudah dan aman serta meningkatkan transaksi antara
            penjual dan pembeli properti untuk membuka usaha maupun kebutuhan lainnya.
          </p>
        </div>
        <div class="col-lg-5 offset-lg-1 col-md-6 py-4 text-center">
          <h3>Misi</h3>
          <p>
            Jajan Properti menyediakan banyak pilihan properti yang terpercaya, aman dan terjangkau. bersamaan dengan moto kami yaitu membangun dan menjembatani bisnis properti agar lebih mudah dan aman serta meningkatkan transaksi antara
            penjual dan pembeli properti untuk membuka usaha maupun kebutuhan lainnya.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 my-4">
          <div class="text-center">
            <h3>Official Partners</h3>
          </div>
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
      </div>
      <div class="row my-4">
        <div class="col-md-4 offset-md-2 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-center my-4">
          <h3>Contact Us</h3>
        </div>

        <div class="col-lg-3 offset-lg-1 col-md-6 text-center">
          <h4>Head Office</h4>
          <p>Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
        </div>
        <div class="col-lg-4 col-md-6 text-center">
          <h4>Social Media</h4>
          <p>Facebook : Jajan Properti</p>
          <p>Website : jajanproperti.com</p>
          <p>Mail : HalloJpro@gmail.com</p>
        </div>
        <div class="col-lg-3 col-md-12 text-center">
          <h4>Sub Office</h4>
          <p>Bandung : Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
          <p>Jakarta : Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
          <p>Jogja : Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
        </div>
      </div>
    </div>
    <!-- Akhir Jumbotorn -->

    <!-- Footer -->
    <div class="container-fluid border" style="background-color: #1c145f">
      <div class="row">
        <div class="col-12 text-white text-center py-4">
          <p>copyright@semongkoteam2021</p>
        </div>
      </div>
    </div>
    <!-- Akhir Footer -->
    <script src="node_modules/jquery/dist/jquery.min.js" crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
