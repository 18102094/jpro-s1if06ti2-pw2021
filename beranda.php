<?php 
  include "connection.php";
  $qproperti = "select * from properti";
  $data_properti = $conn->query($qproperti);
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/logo.png" />
    <title>Selamat datang di Jajan Pro</title>
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
  </head>

  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1c145f">
      <div class="container">
        <a class="navbar-brand" href="#"> <img src="img/logo.png" width="90" height="50" class="d-inline-block align-top" alt="" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Buy<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="rent.php">Rent</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="addproperti.php">Input Properties</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Others </a>
              <div class="dropdown-menu text-light" style="background-color: #1c145f" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item text-light" href="aboutus.php">About Us</a>
                <a class="dropdown-item text-light" href="panduan_pengguna.php">User Manual</a>
                <a class="dropdown-item text-light" href="#">Report and Feedback</a>
              </div>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-3" type="search" placeholder="Search" aria-label="Search" />
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Jumbotorn -->
    <div class="jumbotron jumbotron-fluid bg-primary">
      <div class="container">
        <h2 class="display-4 text-light"><b> Selamat Datang,</b></h2>
        <p class="lead text-light"><b>Jajanproerti.com</b> merupakan situs jual, beli, dan sewa properti yang terjangkau dan terpercaya</p>
      </div>
    </div>
    <!-- Akhir Jumbotorn -->

    <!-- Content -->
    <div class="container">
      <div class="row">
        <?php
            foreach($data_properti as $index => $value){
         ?>
        <div class="col-12 col-md-4">
          <div class="card mb-3 border">
            <img class="card-img-top" src="uploads/<?php echo $value['foto']; ?>" alt="Card image properti" width="180px" height="200px" />
            <div class="card-body">
              <h5 class="card-title"><?php echo $value['nama'] ?></h5>
              <p class="card-text"><?php echo $value['fasilitas'] ?></p>
              <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
              <div>
                <a href="detail.php?properti_id=<?php echo $value['properti_id'] ?>" class="btn btn-primary" style="width: 90px; margin-bottom: 20px">Detail</a>
              </div>
            </div>
          </div>
        </div>
        <?php
              }
          ?>
      </div>
    </div>
    <!-- Akhir Content -->
    <script src="node_modules/jquery/dist/jquery.min.js" crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
