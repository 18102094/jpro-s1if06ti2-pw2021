<?php include "connection.php" ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="aset/style.css" />

  <title>Sign in & Sign up Form</title>

</head>

<body>
  <div class="container">
    <div class="forms-container">
      <div class="signin-signup">

        <!-- FORM LOGIN -->
        <form action="login.php" method="POST" class="sign-in-form">

          <?php if (isset($_GET['error'])) { ?>
          <p class="error">
            <?php echo $_GET['error']; ?>
          </p>
          <?php } ?>

          <h2 class="title">Sign in</h2>

          <!-- INPUT USERNAME -->
          <div class="input-field">
            <i class="fas fa-user"></i>
            <input type="text" name="username" placeholder="Username" />
          </div>

          <!-- INPUT PASSWORD -->
          <div class="input-field">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" placeholder="Password" />
          </div>

          <!-- BTN SUBMIT -->
          <input type="submit" value="Login" class="btn solid" />

          <!-- LOGIN DENGAN AKUN SOSIAL -->
          <p class="social-text">Or Sign in with social platforms</p>
          <div class="social-media">
            <a href="#" class="social-icon">
              <i class="fab fa-facebook-f"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-twitter"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-google"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-linkedin-in"></i>
            </a>
          </div>
        </form>


        <!-- FORM REGISTER -->
        <form action="add_user.php" method="POST" class="sign-up-form">

          <?php if (isset($_GET['error'])) { ?>
          <p class="error">
            <?php echo $_GET['error']; ?>
          </p>
          <?php } ?>

          <h2 class="title">Sign up</h2>

          <!-- INPUT USERNAME -->
          <div class="input-field">
            <i class="fas fa-user"></i>
            <input type="text" name="username" placeholder="Username" />
          </div>

          <!-- INPUT EMAIL -->
          <div class="input-field">
            <i class="fas fa-envelope"></i>
            <input type="email" name="email" placeholder="Email" />
          </div>

          <!-- INPUT PASSWORD -->
          <div class="input-field">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" placeholder="Password" />
          </div>

          <!-- BTN SUBMIT -->
          <input type="submit" class="btn" value="Sign up" />

          <!-- LOGIN DENGAN AKUN SOSIAL -->
          <p class="social-text">Or Sign up with social platforms</p>
          <div class="social-media">
            <a href="#" class="social-icon">
              <i class="fab fa-facebook-f"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-twitter"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-google"></i>
            </a>
            <a href="#" class="social-icon">
              <i class="fab fa-linkedin-in"></i>
            </a>
          </div>
        </form>
      </div>
    </div>

    <!-- PANEL -->
    <div class="panels-container">
      <div class="panel left-panel">
        <div class="content">
          <h3>jajan Properti</h3>
          <p>
            jajan properti merupakan situs jual beli properti aman, mudah, dan terpercaya
          </p>
          <button class="btn transparent" id="sign-up-btn">
            Sign up
          </button>
        </div>
        <img src="img/log.svg" class="image" alt="" />
      </div>
      <div class="panel right-panel">
        <div class="content">
          <h3>jajan Properti</h3>
          <p>
            jajan properti merupakan situs jual beli properti aman, mudah, dan terpercaya
          </p>
          <button class="btn transparent" id="sign-in-btn">
            Sign in
          </button>
        </div>
        <img src="img/register.svg" class="image" alt="" />
      </div>
    </div>
  </div>

  <script src="aset/app.js"></script>
</body>

</html>