<?php 
  include "connection.php";
  $qpropertisewa = "select * from propertisewa";
  $data_propertisewa = $conn->query($qpropertisewa);

  $qselect_propertisewa = "select
  *
  from propertisewa
  left join kategori on kategori.kategori_id = propertisewa.kategori_id
  where propertisewa_id = ".$_GET['propertisewa_id'];
  foreach($conn->query($qselect_propertisewa) as $value){
  $data_select_propertisewa=$value;
  } ;
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/logo.png" />
    <title>Selamat datang di Jajan Pro</title>
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
  </head>

  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark sticky-top mb-3" style="background-color: #1c145f">
      <div class="container">
        <a class="navbar-brand" href="#"> <img src="img/logo.png" width="90" height="50" class="d-inline-block align-top" alt="" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="beranda.php">Buy<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="rent.php">Rent</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="addproperti.php">Input Properties</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Others </a>
              <div class="dropdown-menu text-light" style="background-color: #1c145f" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item text-light" href="aboutus.php">About Us</a>
                <a class="dropdown-item text-light" href="panduan_pengguna.php">User Manual</a>
                <a class="dropdown-item text-light" href="#">Report and Feedback</a>
              </div>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-3" type="search" placeholder="Search" aria-label="Search" />
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Content -->
    <div class="container">
      <div class="row mt-5">
        <div class="col-12 col-md-10 offset-md-1">
          <div class="card mb-3">
            <img class="card-img-top" src="uploads2/<?php echo $value['foto']; ?>" alt="Card image cap" />
            <div class="card-body">
            <input type="hidden" name="propertisewa_id" value="<?php echo $data_select_propertisewa['propertisewa_id'] ?>">
              <h2 class="Display-2 mb-3"><?php echo $data_select_propertisewa['nama'] ?></h2>
              <hr>
              <h5 class="card-title mb-3"><?php echo $data_select_propertisewa['harga'] ?></h5>

              <p class="mb-3"> <b> Lokasi Properti : </b> <?php echo $data_select_propertisewa['alamat'] ?> </p>
              <p class="mb-3"> <b> Fasilitas : </b> <?php echo $data_select_propertisewa['fasilitas'] ?> </p>
              <p class="mb-3"> <b> Deskripsi : </b> <?php echo $data_select_propertisewa['deskripsi'] ?>  </p>
              <div>
                <a href="#" class="btn btn-primary mr-3 " data-toggle="modal" data-target="#hubungi" style="width: 90px; margin-bottom: 20px">Hubungi</a>
                <a href="beranda.php" class="btn btn-warning" style="width: 90px; margin-bottom: 20px">Kembali</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="hubungi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Silahkan Hubungi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <input type="hidden" name="propertisewa_id" value="<?php echo $data_select_propertisewa['propertisewa_id'] ?>">
          <p class="mb-3"> <b> Telepon/WA : </b> <?php echo $data_select_propertisewa['telepon'] ?> </p>
          <p class="mb-3"> <b> Email : </b>  <?php echo $data_select_propertisewa['email'] ?> </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
  </div>
    <!-- Akhir Modal -->
    <!-- Akhir Content -->
    <script src="node_modules/jquery/dist/jquery.min.js" crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
