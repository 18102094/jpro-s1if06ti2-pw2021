<?php include "connection.php";
$qkategori = "select * from kategori";
$data_kategori = $conn->query($qkategori); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/logo.png" />
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <title>Jajan Properti | Input Properti</title>
  </head>

  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1c145f">
      <div class="container">
        <a class="navbar-brand" href="#"> <img src="img/logo.png" width="90" height="50" class="d-inline-block align-top" alt="" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="beranda.php">Buy<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="rent.php">Rent</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">Input Properties</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Others </a>
              <div class="dropdown-menu text-light" style="background-color: #1c145f" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item text-light" href="aboutus.php">About Us</a>
                <a class="dropdown-item text-light" href="panduan_pengguna.php">User Manual</a>
                <a class="dropdown-item text-light" href="#">Report and Feedback</a>
              </div>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-3" type="search" placeholder="Search" aria-label="Search" />
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Jumbotorn -->
    <div class="jumbotron jumbotron-fluid bg-primary">
      <div class="container">
        <h3 class="display-6 text-light text-center"><b> Isi Form Berikut Untuk Menambah Iklan Anda</b></h3>
      </div>
    </div>
    <!-- Akhir Jumbotorn -->
    <div class="row">
      <div class="col-2 offset-5 text-end">
        <a href="addproperti.php" class="btn btn-primary" style="width: 100px; margin-bottom: 20px">PROPERTI JUAL</a>
        <a href="#" class="btn btn-primary" style="width: 100px; margin-bottom: 20px">PROPERTI SEWA</a>
      </div>
    </div>

    <!-- Content -->
    <div class="col-md-6 col-10 offset-md-3 offset-1 border p-5 shadow p-3 mb-5 bg-white rounded">
      <form action="simpan_sewa.php" method="POST" enctype="multipart/form-data">
        <div class="text-center mb-4">
          <h5 class="text-center">MASUKAN PROPERTI YANG AKAN ANDA SEWA</h5>
        </div>
        <div class="mb-3">
          <label for="nama">Nama Properti</label>
          <input type="text" class="form-control" placeholder="Masukan Judul" name="nama" id="nama" />
        </div>
        <div class="mb-3">
          <label for="kategori">Kategori</label>
          <select class="custom-select d-block w-100" id="kategori" name="kategori_id" required>
            <option value="">Pilih...</option>
            <?php
                            foreach($data_kategori as $index =>
            $value){ ?>
            <option value="<?php echo $value['kategori_id'] ?>"><?php echo $value['nama_kategori'] ?></option>
            <?php
                            }
                        ?>
          </select>
        </div>
        <div class="mb-3">
          <label for="alamat">Alamat</label>
          <input type="text" class="form-control" placeholder="Masukan Alamat" name="alamat" id="alamat" />
        </div>
        <div class="mb-3">
          <label for="harga">Harga</label>
          <input type="text" class="form-control" placeholder="Masukan Harga" name="harga" id="harga" />
        </div>
        <div class="mb-3">
          <label for="telepon">Telepon</label>
          <input type="text" class="form-control" placeholder="Masukan Terlepon" name="telepon" id="telepon" />
        </div>
        <div class="mb-3">
          <label for="email">Email</label>
          <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Masukan email" name="email" id="email" />
        </div>
        <div class="mb-3">
          <label for="fasilitas">Fasilitas</label>
          <textarea class="form-control" rows="3" name="fasilitas" id="fasilitas"></textarea>
        </div>
        <div class="mb-3">
          <label for="deskripsi">Deskripsi</label>
          <textarea class="form-control" rows="3" name="deskripsi" id="deskripsi"></textarea>
        </div>
        <div class="mb-3">
          <label for="gambar">Pilih Gambar</label>
          <input type="file" class="form-control-file" id="gambar" name="gambar" />
        </div>
        <button type="submit" class="btn btn-primary">Simpan Data</button>
      </form>
    </div>

    <!-- END Content -->
    <script src="node_modules/jquery/dist/jquery.min.js" crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>
