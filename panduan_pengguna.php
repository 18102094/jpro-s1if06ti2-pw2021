<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/logo.png" />
    <title>Selamat datang di Jajan Pro</title>
    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  </head>

  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark sticky-top" style="background-color: #1c145f">
      <div class="container">
        <a class="navbar-brand" href="#"> <img src="img/logo.png" width="90" height="50" class="d-inline-block align-top" alt="" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="beranda.php">Buy<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="rent.php">Rent</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="addproperti.php">Input Properties</a>
            </li>
            <li class="nav-item dropdown active">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Others </a>
              <div class="dropdown-menu text-light" style="background-color: #1c145f" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item text-light" href="aboutus.php">About Us</a>
                <a class="dropdown-item text-light" href="panduan_pengguna.php">User Manual</a>
                <a class="dropdown-item text-light" href="#">Report and Feedback</a>
              </div>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-3" type="search" placeholder="Search" aria-label="Search" />
            <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Jumbotorn -->
    <div class="jumbotron jumbotron-fluid bg-primary">
      <div class="container">
        <h2 class="display-4 text-light text-center"><b> User Manual</b></h2>
        <p class="lead text-light text-center"><b>Jajanproerti.com</b></p>
      </div>
    </div>
    <!-- Akhir Jumbotorn -->
    <!-- MENU USER MANUAL -->
    <div class="w3-container" id="menu">
      <div class="w3-content" style="max-width: 1000px">
        <div class="w3-row w3-center w3-card w3-padding">
          <a href="javascript:void(0)" onclick="openMenu(event, 'administration');" id="myLink">
            <div class="w3-col s6 tablink">Login and Register</div>
          </a>

          <a href="javascript:void(0)" onclick="openMenu(event, 'create');">
            <div class="w3-col s6 tablink">Create Article</div>
          </a>
        </div>

        <!-- CONTENT USER MANUAL -->

        <!-- Content Login and Registration -->
        <div id="administration" class="w3-container menu w3-padding-48 w3-card">
          <h2 class="text-center">Login and Registration</h2>
          <br />
          <p class="text-center">
            Pada halaman register, user melakukan pendaftaran akun pada web jpro , user melakukan pengisian username password dan gmail, lalu user akan menerima authentication melalui gmail yang sudah diiputkan user dan mengkonfirmasi akun
            tersebut, jika sudah mengkonfirmasi user dapat melanjutkan login pada web jpro
          </p>
          <p class="text-center">
            Pada halaman login user dapat melakukan login melalui akun yang sudah di didaftarkan di web jpro dengan mengisikan username password yang sudah diisikan user pada saat registrasi dan sudah di authentication pada gmail yang sudah
            diinputkan user untuk mengkonfirmasi apakah benar gmail itu milik user tersebut , jika belum mempunyai akun user dapat melakukan pendaftaran akun pada kolom register
          </p>
        </div>

        <!-- Content Create Article -->
        <div id="create" class="w3-container menu w3-padding-48 w3-card">
          <h2 class="text-center">Create Article</h2>
          <br />
          <p class="text-center">
            Create article pada fitur ini dilakukan melalui ajukan properti diperuntukan user yang ingin memposting/mengiklankan properti mereka yang ingin dijual atau disewakan, user dapat mengisi form pada halaman ajukan properti user
            melakukan input data yaitu judul, kategory, alamat, no telepon, email, fasilitas, status dan dekripsi. Jika sudah benar user dapat melakukan submit untuk memposting properti yang akan mereka jual atau sewa.
          </p>
        </div>
      </div>
    </div>

    <!-- END MENU USER MANUAL -->

    <!-- CONTENT USER MANUAL -->
    <div class="row">
        <div class="col-12 my-4">
          <div class="text-center">
            <h3>Official Partners</h3>
          </div>
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
      </div>
      <div class="row my-4">
        <div class="col-md-4 offset-md-2 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
        <div class="col-md-4 mt-3">
          <img src="img/kantor.jpg" class="img-fluid" />
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-center my-4 text">
          <h3>Contact Us</h3>
        </div>

        <div class="col-lg-3 offset-lg-1 col-md-6 text-center">
          <h4>Head Office</h4>
          <p>Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
        </div>
        <div class="col-lg-4 col-md-6 text-center">
          <h4>Social Media</h4>
          <p>Facebook : Jajan Properti</p>
          <p>Website : jajanproperti.com</p>
          <p>Mail : HalloJpro@gmail.com</p>
        </div>
        <div class="col-lg-3 col-md-12 text-center">
          <h4>Sub Office</h4>
          <p>Bandung : Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
          <p>Jakarta : Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
          <p>Jogja : Jl. Oversite Isdiman G.VII No.3, Purwokerto Timur, Banyumas, Indonesia</p>
        </div>
      </div>
    </div>
    <!-- Akhir Jumbotorn -->
    <!-- FOOTER -->
    <div class="container-fluid border" style="background-color: #1c145f">
        <div class="row">
        <div class="col-12 text-white text-center py-4">
            <p>copyright@semongkoteam2021</p>
        </div>
        </div>
    </div>
    <!-- Akhir Content -->

    <script src="node_modules/jquery/dist/jquery.min.js" crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
        // TABBED MENU
        function openMenu(evt, menuName) {
          var i, x, tablinks;
          x = document.getElementsByClassName("menu");
          for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
          }
          tablinks = document.getElementsByClassName("tablink");
          for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-blue", "");
          }
          document.getElementById(menuName).style.display = "block";
          evt.currentTarget.firstElementChild.className += " w3-blue";
        }
        document.getElementById("myLink").click();
        </script>
  </body>
</html>
